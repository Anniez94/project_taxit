const modal =  document.querySelectorAll(".modals");
const modalBtn = document.querySelector("#tbook");
const modalManagerBtn = document.querySelector("#taxitmanager");
const modalTaxitBtn = document.querySelector("#taxitwht");
const modalTaxitPayBtn = document.querySelector("#taxitpay");
const modalTaxitPayrollBtn = document.querySelector("#taxitpayroll");
const closeBtn = document.querySelectorAll(".closebtn");
const citapp = document.querySelector("#citapp");
const ussd = document.querySelector("#ussd")

const openModal = () => {
    modal[0].classList.add('modal-active');
}
const openPayrollModal = () => {
    modal[1].classList.add('modal-active');
}
const openPayModal = () => {
    modal[2].classList.add('modal-active');
}
const openWitholdModal = () => {
    modal[3].classList.add('modal-active');
}
const openCitappModal = () => {
    modal[4].classList.add('modal-active');
}
const openManagerModal = () => {
    modal[5].classList.add('modal-active');
}
const openUssdModal = () => {
    modal[6].classList.add('modal-active');
}

const closeModal = () => {
    modal[0].classList.remove('modal-active');
};
const closeModal2 = () => {
        modal[1].classList.remove('modal-active'); 
};
const closeModal3 = () => {
    modal[2].classList.remove('modal-active'); 
};
const closeModal4 = () => {
    modal[3].classList.remove('modal-active'); 
};
const closeModal5 = () => {
    modal[4].classList.remove('modal-active'); 
};
const closeModal6 = () => {
    modal[5].classList.remove('modal-active'); 
};
const closeModal7 = () => {
    modal[6].classList.remove('modal-active'); 
};

modalBtn.addEventListener("click", openModal);
modalTaxitBtn.addEventListener("click", openWitholdModal);
modalTaxitPayBtn.addEventListener("click", openPayModal)
modalTaxitPayrollBtn.addEventListener("click", openPayrollModal);
closeBtn[0].addEventListener("click", closeModal);
closeBtn[1].addEventListener("click", closeModal2);
closeBtn[2].addEventListener("click", closeModal3);
closeBtn[3].addEventListener("click", closeModal4);
closeBtn[4].addEventListener("click", closeModal5);
closeBtn[5].addEventListener("click", closeModal6);
closeBtn[6].addEventListener("click", closeModal7);
citapp.addEventListener("click", openCitappModal);
modalManagerBtn.addEventListener("click", openManagerModal);
ussd.addEventListener("click", openUssdModal)

